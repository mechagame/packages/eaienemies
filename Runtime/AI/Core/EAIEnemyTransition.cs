﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    [Serializable]
    public class EAIEnemyTransition
    {
        public EAIEnemyDecision decision;
        public EAIEnemyState TrueState;
        public EAIEnemyState FalseState;
    }
}