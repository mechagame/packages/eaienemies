﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    public abstract class EAIEnemyDecision : ScriptableObject
    {
        public abstract bool EnemyDecide(EAIStateController stateController);
    }
}