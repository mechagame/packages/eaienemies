﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyData",menuName = "Enemy AI/Enemy Data")]
    public class EAIEnemyData : ScriptableObject
    {
        [Header("Setting Agent NavMesh")]
        public float moveSpeed = 4f;
        public float stoppingDistance=10f;
        // public float lookRange = 20f;

        [Header("Setting Detection Target")]
        public float lookSphereCastRadius = 15f;
        //public float searchingTurnSpeed = 4f;
        //public float searchDuration = 120f;

        [Header("Setting Attack Target")]
        public float attackRange = 15f;
        public float attackRate = 0.5f;
        public int attackDamage = 50;

      
    }
}