﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//|                                                                             This Library is made For Mecha Nichon                                                                       | 
//|                                                                                                                                                                                       |
//|                                                                                 Copyright Mecha Boullette 2020                                                                           | 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AftahGames.HelperLibrary;



namespace AftahGames
{
    public class EAIManager :  Singleton <EAIManager>
    {
        #region SERIALIZED FIELDS
        [SerializeField] private  Transform targetToChase;
        [SerializeField] private LayerMask targetLayerMask;

        #endregion

        #region PRIVATE FIELDS
        #endregion

        #region PUBLIC PROPERTIES

        public  Transform GetTargetToChase { get { return targetToChase; } }
        public LayerMask GetTargetLayerMask { get { return targetLayerMask; } }

        #endregion

        #region PUBLIC FUNCTIONS

        #endregion

        #region EVENTS
        #endregion

        #region PRIVATE FUNCTIONS

        // Start is called before the first frame update
        void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
        #endregion


    }
}
