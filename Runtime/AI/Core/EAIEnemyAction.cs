﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    public abstract class EAIEnemyAction : ScriptableObject
    {
        public abstract void EnemyAction(EAIStateController stateController);
    }
}