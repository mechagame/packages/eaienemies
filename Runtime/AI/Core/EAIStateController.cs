﻿using AftahGames.HelperLibrary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AftahGames
{

    public class EAIStateController : MonoBehaviour
    {
        [Header("State")]
        [SerializeField] private EAIEnemyState currentState;
        [SerializeField] private EAIEnemyState remainState;
      

        [Header("Enemy Data")]
        [SerializeField] private EAIEnemyData enemyData;

        [HideInInspector] public float stateTimeElapsed;

        

        public Transform ChaseTarget { get; set; }
        public float WanderCheckTime { get; set; }
        public NavMeshAgent Agent { get; set; }
        public Path Path { get; set; }
        public EAIEnemyData EnemyData { get; set; }
        public EAIEnemyShoot EnemyShoot { get; set; }
        public Animator EnemyAnimator { get; set; }


        private void Awake()
        {
            Agent = GetComponent<NavMeshAgent>();
            Path = GetComponent<Path>();
            EnemyShoot = GetComponentInChildren<EAIEnemyShoot>();
            EnemyAnimator = GetComponent<Animator>();
            EnemyData = enemyData;
          
            ChaseTarget = EAIManager.Instance.GetTargetToChase;                                  

        }

        private void OnEnable()
        {
            WanderCheckTime = 0;
            Agent.speed = EnemyData.moveSpeed;
            Agent.stoppingDistance = enemyData.stoppingDistance;
            
        }

        // Update is called once per frame
        void Update()
        {

            currentState.EvaluateState(this);
        }

        public void TransitionToState(EAIEnemyState nextState)
        {
            if (nextState != remainState)
            {
                currentState = nextState;
            }
        }

        public bool CheckIfCountDownElapsed(float duration)
        {
            stateTimeElapsed += Time.deltaTime;
            return (stateTimeElapsed >= duration);
        }

        private void OnExitState()
        {
            stateTimeElapsed = 0;
        }
    }
}