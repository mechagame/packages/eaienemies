﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyAIState",menuName = "Enemy AI/State")]
    public class EAIEnemyState : ScriptableObject
    {
        public EAIEnemyAction[] enemyActions;
        public EAIEnemyTransition[] enemyTransitions;

        public void EvaluateState(EAIStateController stateController)
        {

            EnemyDoActions(stateController);
            EvaluateTransition(stateController);
        }

        public void EnemyDoActions(EAIStateController stateController)
        {

            foreach (EAIEnemyAction act in enemyActions)
            {
                act.EnemyAction(stateController);
            }

        }

        public void EvaluateTransition(EAIStateController stateController)
        {
            if (enemyTransitions != null || enemyTransitions.Length > 1)
            {
                for (int i = 0; i < enemyTransitions.Length; i++)
                {
                    bool DecisionResult = enemyTransitions[i].decision.EnemyDecide(stateController);

                    if (DecisionResult)
                    {
                        stateController.TransitionToState(enemyTransitions[i].TrueState);
                    }
                    else
                    {
                        stateController.TransitionToState(enemyTransitions[i].FalseState);
                    }
                }
            }
        }
    }
}