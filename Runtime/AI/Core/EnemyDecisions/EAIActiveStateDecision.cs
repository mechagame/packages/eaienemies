﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AftahGames
{
    [CreateAssetMenu(menuName = "PluggableAI/Decisions/ActiveState")]
    public class EAIActiveStateDecision : EAIEnemyDecision
    {
        public override bool EnemyDecide(EAIStateController stateController)
        {
            bool chaseTargetIsActive = stateController.ChaseTarget.gameObject.activeSelf;
            return chaseTargetIsActive;
        }
    }

}