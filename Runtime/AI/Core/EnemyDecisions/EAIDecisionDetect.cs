﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyDecisionDetect",menuName = "AI/Decisions/Detect Target")]
    public class EAIDecisionDetect : EAIEnemyDecision
    {
       // public LayerMask targetLayer;
       
        public override bool EnemyDecide(EAIStateController stateController)
        {
            return CheckTarget(stateController);
        }

        private bool CheckTarget(EAIStateController stateController)
        {
            if (stateController == null)
            {
                return false;
            }
            Collider[] colliders;

            colliders = Physics.OverlapSphere(stateController.transform.position,stateController.EnemyData.lookSphereCastRadius,EAIManager.Instance.GetTargetLayerMask );

            if (colliders.Length > 0)
            {
                stateController.ChaseTarget = colliders[0].transform;

                return true;
            }

            return false;


        }
    }
}