﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyDecisionRangeToAttack",menuName = "AI/Decisions/Detect Range To Attack")]
    public class EAIDecisionRangeToAttack : EAIEnemyDecision
    {
      

        public override bool EnemyDecide(EAIStateController stateController)
        {
            return PlayerInRangeToAttack(stateController);
        }

        private bool PlayerInRangeToAttack(EAIStateController stateController)
        {
            if (stateController.ChaseTarget != null)
            {
                //disatnce the target
                float distanceToAttack = (stateController.ChaseTarget.position - stateController.transform.position).sqrMagnitude;

                //true If we are close to the target
                return distanceToAttack < Mathf.Pow(stateController.EnemyData.stoppingDistance,2);
            }
            return false;
        }
    }
}