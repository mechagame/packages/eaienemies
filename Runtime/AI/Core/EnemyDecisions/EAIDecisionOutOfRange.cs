﻿using AftahGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyDecisionOutOfRangeToAttack",menuName = "AI/Decisions/Detect Out Of Range To Attack")]
    public class EAIDecisionOutOfRange : EAIEnemyDecision
    {
        public override bool EnemyDecide(EAIStateController stateController)
        {
            return PlayerOutOfRangeToAttack(stateController);
        }

        private bool PlayerOutOfRangeToAttack(EAIStateController stateController)
        {
            if (stateController.ChaseTarget != null)
            {
                //disatnce the target
                float distanceToAttack = (stateController.ChaseTarget.position - stateController.transform.position).sqrMagnitude;

                //true If we are far to the target
                return distanceToAttack > Mathf.Pow(stateController.EnemyData.stoppingDistance,2);
            }
            return false;
        }
    }
}
