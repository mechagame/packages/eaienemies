﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//|                                                                             This Library is made by Abdelfetah Hamra                                                                      | 
//|                                                                                                                                                                                       |
//|                                                                                 Copyright Aftah-Games 2020                                                                           | 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames.HelperLibrary
{
    public class Singleton<T> : MonoBehaviour where T : Component
    {

        #region PRIVATE FIELDS

        private static T instance;

        #endregion

        #region PUBLIC PROPERTIES

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();
                    if (instance == null)
                    {
                        GameObject newGameObject = new GameObject();
                        instance = newGameObject.AddComponent<T>();
                    }
                }
                return instance;
            }

        }

        #endregion

        #region PROTECTED FUNCTIONS
        protected virtual void Awake()
        {
            instance = this as T;
        }

        #endregion
    }
}
