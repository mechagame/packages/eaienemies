﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//|                                                                             This Library is made by Abdelfetah Hamra                                                                      | 
//|                                                                                                                                                                                       |
//|                                                                                 Copyright Aftah-Games 2020                                                                           | 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames.HelperLibrary
{
    public class ReturnToPool : MonoBehaviour
    {
        #region SERIALIZED FIELDS

      
        [SerializeField] private float lifeTime = 1.5f;

        #endregion

        #region PRIVATE FUNCTIONS

        private void ReturnTo()
        {

            gameObject.SetActive(false);
        }
        private void OnEnable()
        {
            Invoke("ReturnTo",lifeTime);
        }


        private void OnDisable()
        {
            CancelInvoke();
        }

      
        #endregion
    }
}
