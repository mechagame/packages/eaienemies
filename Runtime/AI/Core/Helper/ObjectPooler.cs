﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//|                                                                             This Library is made by Abdelfetah Hamra                                                                      | 
//|                                                                                                                                                                                       |
//|                                                                                 Copyright Aftah-Games 2020                                                                           | 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames.HelperLibrary
{


    public class ObjectPooler : MonoBehaviour
    {
        #region SERIALIZED FIELDS

        [SerializeField] private string childHierarchyName = "EnemyAIPool";
        [SerializeField] private GameObject objectPrefab;

        [SerializeField] private float poolSize = 10f;

        [SerializeField]
        private bool poolCanExpand = true;
        #endregion

        #region PRIVATE FIELDS

        private List<GameObject> poolGameObjects;
       
        private GameObject childObject;

        #endregion

        #region PRIVATE FUNCTIONS

        // Start is called before the first frame update
        void Start()
        {
            Refill();
           
        }

        private void Refill()
        {
           
            childObject = new GameObject(childHierarchyName);
          //  childObject.transform.SetParent(this.gameObject.transform.parent);

            poolGameObjects = new List<GameObject>();

            for (int i = 0; i < poolSize; i++)
            {
                AddObjectToPool();
              
            }
        }


        private GameObject AddObjectToPool()
        {
            GameObject newObj = Instantiate(objectPrefab);
            newObj.SetActive(false);
          
           newObj.transform.SetParent( childObject.transform);

            poolGameObjects.Add(newObj);
            return newObj;
        }
        #endregion


    #region PUBLIC FUNCTIONS
        public GameObject GetObjectFormPool()
        {
            for (int i = 0; i < poolGameObjects.Count; i++)
            {
                if (!poolGameObjects[i].activeInHierarchy)
                {
                    return poolGameObjects[i];
                }
            }

            if (poolCanExpand)
            {
                return AddObjectToPool();
            }

            return null;
        }
    #endregion

    }
}