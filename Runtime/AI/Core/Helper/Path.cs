﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//|                                                                             This Library is made by Abdelfetah Hamra                                                                      | 
//|                                                                                                                                                                                       |
//|                                                                                 Copyright Aftah-Games 2020                                                                          | 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames.HelperLibrary
{
    public class Path : MonoBehaviour
    {
        #region SERIALIZED FIELDS

        [Header("Setting")]
        [SerializeField] private List<Vector3> path;
      

        #endregion

        #region PRIVATE FIELDS

        private Vector3 currentPosition;
        private Vector3 startPosition;
        private IEnumerator<Vector3> currentPoint;
        private float distanceToPoint;
        private bool gameStarted;

        #endregion

        #region PUBLIC PROPERTIES

        public Vector3 CurrentPoint { get { return startPosition + currentPoint.Current; } }

        #endregion

        #region PRIVATE FUNCTIONS
        // Start is called before the first frame update
        void Awake()
        {
            gameStarted = true;
            startPosition = transform.position;
            currentPosition = transform.position;

            currentPoint = GetPoint();
            currentPoint.MoveNext();

            transform.position = transform.position + currentPoint.Current;
        }

        // Update is called once per frame
        void Update()
        {
            if (path != null || path.Count > 0)
            {
                ComputePath();
            }
        }

        private void ComputePath()
        {
            distanceToPoint = (transform.position - (currentPosition + currentPoint.Current)).magnitude;

            if (distanceToPoint < 0.5f)
            {
                currentPoint.MoveNext();
            }
        }

        private void OnDrawGizmos()
        {
            if (!gameStarted && transform.hasChanged)
            {
                currentPosition = transform.position;
            }
            for (int i = 0; i < path.Count; i++)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(currentPosition + path[i],0.2f);

                if (i < path.Count - 1)
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawLine(currentPosition + path[i],currentPosition + path[i + 1]);
                }

                if (i == path.Count - 1)
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawLine(currentPosition + path[i],currentPosition + path[0]);
                }
            }
        }

        #endregion

        #region PUBLIC FUNCTIONS

        public IEnumerator<Vector3> GetPoint()
        {
            int index = 0;

            while (true)
            {
                yield return path[index];

                if (path.Count <= 1)
                {
                    continue;
                }

                index++;

                if (index < 0)
                {
                    index = path.Count - 1;
                }
                else if (index > path.Count - 1)
                {
                    index = 0;
                }
            }
        }

        #endregion

    }
}