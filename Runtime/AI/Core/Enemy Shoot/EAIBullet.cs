﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    public class EAIBullet : MonoBehaviour
    {
        [SerializeField] float bulletSpeed = 12f;

        private void Update()
        {
            transform.Translate(Vector3.forward * bulletSpeed * Time.deltaTime);

        }

     
    }
}
