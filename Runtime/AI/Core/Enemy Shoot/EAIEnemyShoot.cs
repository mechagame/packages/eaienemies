﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AftahGames.HelperLibrary;

namespace AftahGames
{

    public class EAIEnemyShoot : MonoBehaviour
    {

        [Header("Settings")]
        [SerializeField] private Transform shootPosition;

        public ObjectPooler pooler { get; set; }

        private float nextFire;

        private void Start()
        {

        }
        private void Awake()
        {

            pooler = GetComponentInChildren<ObjectPooler>();

        }


        public void Shoot(float fireRate)
        {
            if (Time.time > nextFire)
            {
                SpawnBullet();
                nextFire = Time.time + fireRate;
            }

        }

        private void SpawnBullet()
        {
            if (pooler != null)
            {

                GameObject pool = pooler.GetObjectFormPool();

                pool.transform.position = shootPosition.position;
                pool.transform.rotation = shootPosition.rotation;
                pool.SetActive(true);
            }

        }

    }

}