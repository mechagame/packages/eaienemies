﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace AftahGames
{
    public class EAIEnemyHealth : MonoBehaviour
    {
        [SerializeField] private int maxHealth = 30;

        private int currentHealth;
        public int GetCurrentEnemyHealth { get { return currentHealth; } }
        public int GetMaxEnemyHealth { get { return maxHealth; } }


        public bool EnemyIsDie { get; set; }

        public void TakeDamage(int damage)
        {
            currentHealth -= damage;
            if (currentHealth <= 0)
            {
                EnemyIsDie = true;
                gameObject.SetActive(false);

            }
        }

        private void OnEnable()
        {

            currentHealth = maxHealth;

        }


    }


}