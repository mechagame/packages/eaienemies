﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyActionMeele",menuName = "AI/Actions/Meele")]
    public class EAIActionMelee : EAIEnemyAction
    {
        public override void EnemyAction(EAIStateController stateController)
        {
            Attack(stateController);
        }

        private void Attack(EAIStateController stateController)
        {

            if(stateController == null)
            {
                return;
            }

            //Stop
            stateController.Agent.isStopped = true;

            //Attack &
            //disatnce the target
            float distanceToAttack = (stateController.ChaseTarget.position - stateController.transform.position).sqrMagnitude;

            
            
            if (stateController.EnemyAnimator != null)
            {
                //true If we are close to the target
                if (distanceToAttack < Mathf.Pow(stateController.EnemyData.stoppingDistance,2))
                {

                    stateController.EnemyAnimator.SetBool("IsMelee",true);
                }
                else
                {
                    stateController.EnemyAnimator.SetBool("IsMelee",false);
                }
            }

        }
    }
}