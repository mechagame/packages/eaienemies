﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyActionShoot",menuName = "AI/Actions/Shoot")]
    public class EAIActionShoot : EAIEnemyAction
    {

        public override void EnemyAction(EAIStateController stateController)
        {

            ShootPlayer(stateController);
        }

        private void ShootPlayer(EAIStateController stateController)
        {
            if (stateController.EnemyShoot == null)
            {
                return;
            }

            //RaycastHit hit;

            //Vector3 newDirection = (stateController.ChaseTarget.position - stateController.transform.position).normalized;

            //if (Physics.SphereCast(stateController.transform.position,stateController.EnemyData.lookSphereCastRadius,newDirection,out hit,stateController.EnemyData.attackRange))
            //{
            //    if (stateController.CheckIfCountDownElapsed(stateController.EnemyData.attackRate))
            //    {
            //        stateController.EnemyShoot.Shoot(stateController.EnemyData.attackRate);
            //    }

            //}

            if (stateController == null)
            {
                return;
            }
            Collider[] colliders;

            colliders = Physics.OverlapSphere(stateController.transform.position,stateController.EnemyData.attackRange,EAIManager.Instance.GetTargetLayerMask);

            if (colliders.Length > 0)
            {
                if (stateController.CheckIfCountDownElapsed(stateController.EnemyData.attackRate))
                {
                    stateController.EnemyShoot.Shoot(stateController.EnemyData.attackRate);
                }

                return;
            }

          

        }

    }
}
