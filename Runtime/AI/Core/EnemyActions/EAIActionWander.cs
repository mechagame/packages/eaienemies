﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyActionWander",menuName = "AI/Actions/Wander")]
    public class EAIActionWander : EAIEnemyAction
    {
        [Header("Wander Settings")]  
        public float radiusToWander = 40;
        public float timeForNewPath = 1f; // for calculate new location

  
        public override void EnemyAction(EAIStateController stateController)
        {

            Wander(stateController);

        }
        private void Wander(EAIStateController stateController)
        {
            if (stateController.Agent == null)
            {
                return;
            }

            if (Time.time > stateController.WanderCheckTime)
            {

                NavMeshHit hit;

                Vector3 randomPosition = Random.insideUnitSphere * radiusToWander;
                randomPosition += stateController.transform.position;
                if (NavMesh.SamplePosition(randomPosition,out hit,radiusToWander,NavMesh.AllAreas))
                {
                    stateController.Agent.SetDestination(hit.position);

                }

                stateController.WanderCheckTime = Time.time + timeForNewPath;

            }
        }

    }
}