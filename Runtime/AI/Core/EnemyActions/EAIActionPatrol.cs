﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyActionPatrol",menuName = "AI/Actions/Patrol")]
    public class EAIActionPatrol : EAIEnemyAction
    {

        private Vector3 newDirection;

        

        public override void EnemyAction(EAIStateController stateController)
        {
            Patrol(stateController);
        }

        private void Patrol(EAIStateController stateController)
        {
          

            if (stateController.Agent == null)
            {
                return;
            }

            newDirection = (stateController.Path.CurrentPoint - stateController.transform.position).normalized;

            stateController.Agent.Move(newDirection * stateController.Agent.speed * Time.deltaTime);
            stateController.Agent.ResetPath();
          
           

        }
    }
}