﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyActionIdle",menuName = "AI/Actions/Idle")]
    public class EAIActionIdle : EAIEnemyAction
    {
        public override void EnemyAction(EAIStateController stateController)
        {
            if (stateController.Agent == null)
            {
                return;
            }

            stateController.Agent.isStopped=true;


        }
    }
}