﻿using UnityEngine;

namespace AftahGames
{
    [CreateAssetMenu(fileName = "EnemyActionFollow",menuName = "AI/Actions/Follow")]
    public class EAIActionFollow : EAIEnemyAction
    {


        public override void EnemyAction(EAIStateController stateController)
        {

            FollowTarget(stateController);
            LookAtPlayer(stateController);
            CheckDistance(stateController);

        }


        private void FollowTarget(EAIStateController stateController)
        {
            if (stateController.Agent == null)
            {
                return;
            }


            stateController.Agent.destination = stateController.ChaseTarget.position;


        }

        private void LookAtPlayer(EAIStateController stateController)
        {
            if (stateController != null)
            {

                stateController.transform.LookAt(stateController.ChaseTarget);
                Vector3 currentRot = stateController.transform.rotation.eulerAngles;
                stateController.transform.rotation = Quaternion.Euler(0,currentRot.y,0);

            }
        }

        private void CheckDistance(EAIStateController stateController)
        {

            if (stateController.Agent.remainingDistance < stateController.Agent.stoppingDistance)
            {
                stateController.Agent.isStopped = true;
            }
            else
            {
                stateController.Agent.isStopped = false;
            }
        }
    }
}